<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Jamu Tradisional - Fatu Rahmat</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" href="UAS_PBW_14831_Fatu_R/assets/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="UAS_PBW_14831_Fatu_R/assets/css/style.css">
      
      <!-- Responsive-->
      <link rel="stylesheet" href="UAS_PBW_14831_Fatu_R/assets/css/responsive.css">
      <!-- fevicon -->
      <link href="UAS_PBW_14831_Fatu_R/assets/images/favicon.png" rel="icon">
      <link href="UAS_PBW_14831_Fatu_R/assets/img/favicon.png" rel="apple-touch-icon">  
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="UAS_PBW_14831_Fatu_R/assets/css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--AOS-->
      <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

   <!-- body -->
   <body class="main-layout">
      <!-- loader  -->
      <div class="loader_bg">
         <div class="loader"><img src="UAS_PBW_14831_Fatu_R/assets/images/loading.gif" alt="#" /></div>
      </div>
      <!-- end loader --> 
      <!-- header -->
      <?php include 'header.php'; ?>
      <!-- end header -->

   <!-- main section -->
      <?php include 'main.php'; ?>
      <!-- end main section -->

   <!-- ======= Map Section ======= -->
      <section id="scrolldown" class="map">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.226124171403!2d110.40668581500458!3d-6.9826208703192485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708b4ec52229d7%3A0xc791d6abc9236c7!2sUniversitas%20Dian%20Nuswantoro!5e0!3m2!1sen!2sid!4v1672730365482!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </section>
      <!-- End Map Section -->

   <!-- ======= Footer ======= -->
      <?php include 'footer.php'; ?>
      <!-- End #footer -->

      <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Javascript files--> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/jquery.min.js"></script> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/popper.min.js"></script> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/bootstrap.bundle.min.js"></script> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/jquery-3.0.0.min.js"></script> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/plugin.js"></script> 
<!-- sidebar --> 
      <script src="UAS_PBW_14831_Fatu_R/assets/js/jquery.mCustomScrollbar.concat.min.js"></script> 
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script src="UAS_PBW_14831_Fatu_R/assets/js/custom.js"></script>
      <script src="UAS_PBW_14831_Fatu_R/assets/js/script.js"></script>
<!--AOS-->
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
        <script>
        AOS.init();
        </script>

   </body>
</html>