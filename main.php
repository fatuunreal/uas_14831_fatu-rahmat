<main id="main">
    <!-- carousell item -->
      <?php include 'main-index/carousell.php'; ?>
      <!-- end carousell item -->

   <!-- konten  -->
      <?php include 'main-index/konten.php'; ?>
      <!-- end kontens -->

   <!-- khasiat --> 
      <?php include 'main-index/khasiat.php'; ?>
      <!-- end khasiat -->

   <!-- our topJamu -->
      <?php include 'main-index/topJamu.php'; ?>
      <!-- end our topJamu -->

   <!-- Gallery -->
      <?php include 'main-index/gallery.php'; ?>
      <!-- end Gallery -->

   <!-- About -->
      <?php include 'main-index/about.php'; ?>
      <!-- end About -->
</main>