<header>
         <!-- header inner -->
         <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                  <div class="full">
                     <div class="center-desk">
                        <div class="logo"> <a href="https://www.instagram.com/fatuunreal/" target="_blank">Fatu Rahmat</a> </div>
                     </div>
                  </div>
               </div>
               <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                  <div class="menu-area">
                     <div class="limit-box">
                        <nav class="main-menu">
                           <ul class="menu-area-main">
                              <li class="active"> <a href="#">Home</a> </li>
                              <li> <a href="#konten">Konten</a> </li>
                              <li> <a href="#khasiat"> Khasiat</a> </li>
                              <li> <a href="#topJamu">Jamu Populer</a> </li>
                              <li> <a href="#gallery">Gallery</a> </li>
                              <li> <a href="#contact">Contact</a> </li>
                              <li class="mean-last"> <a href="https://www.linkedin.com/in/fatu-rahmat-2a387425b/" target="_blank"><img src="UAS_PBW_14831_Fatu_R/assets/icon/lin(2).png" alt="#" /></a> </li>
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
   <!-- end header inner --> 
      </header>