<section id="konten" class="konten">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage" data-aos="fade-up">
                     <h2>Apa itu jamu?</h2>
                     <span>Jamu merupakan minuman berkhasiat dari Indonesia sebagai minuman kesehatan, mencegah, dan menyembuhkan berbagai penyakit. Jamu disajikan dengan berbagai jenis, mengingat di Indonesia memiliki tanaman herbal berjumlah cukup banyak. Setiap daerah mempunyai jenis Jamu yang berbeda, menyesuaikan dengan tanaman herbal yng tumbuh didaerahnya </span>
                  </div>
               </div>
            </div>
         </div>
      
      <div id="sejarah" class="solo" data-aos="fade-up">
         <h2>Sejarah Jamu</h2>
      </div>
         <div class="container-fluid" data-aos="fade-up">
            <div class="row flexcss">
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                  <div class="konten-img">
                    <figure><img src="UAS_PBW_14831_Fatu_R/assets/images/s_8.jpg" alt="img"/></figure>
                    <p class="p0">Masyarakat Indonesia sejak zaman Kerajaan Mataram hingga kini masih menggunakan Jamu. 
                        Minuman khas Indonesia ini telah menjadi kebanggaan tersendiri seperti halnya dengan 
                        Ayurveda dari India dan Zhongyi dari Cina. Sejak saat itu, perempuan lebih berperan dalam 
                        memproduksi jamu, sedangkan pria berperan mencari tumbuhan herbal alami. Fakta itu diperkuat 
                        dengan adanya temuan artefak Cobek dan Ulekan – alat tumbuk untuk membuat jamu. Artefak itu bisa 
                        dilihat di situs arkeologi Liyangan yang berlokasi di lereng Gunung Sindoro, Jawa Tengah.
                     </p>
                     <p>Selain artefak Cobek dan Ulekan, ditemukan juga bukti-bukti lain seperti alat-alat 
                        membuat jamu yang banyak ditemukan di Yogyakarta dan Surakarta, tepatnya di Candi 
                        Borobudur pada relief Karmawipangga, Candi Prambanan, Candi Brambang, dan beberapa lokasi 
                        lainnya. Konon, di zaman dulu, rahasia kesehatan dan kesaktian para pendekar dan petinggi-petinggi 
                        kerajaan berasal dari latihan dan bantuan dari ramuan herbal.</p>
                     <p>Seiring perkembangannya, tradisi minum Jamu sempat mengalami penurunan. 
                        Tepatnya saat pertama kali ilmu modern masuk ke Indonesia. Saat itu kampanye 
                        obat-obatan bersertifikat sukses mengubah pola pikir masyarakat Indonesia sehingga 
                        minat terhadap Jamu menurun. Selain soal standar atau sertifikat, khasiat dari Jamu pun turut dipertanyakan.
                        Mesti tak bersetifikat, khasiat Jamu telah teruji oleh 
                        waktu secara turun-temurun digunakan sebagai obat tradisional. Sehingga hingga saat ini, minuman berkhasiat 
                        khas Indonesia ini selalu terjaga keberlangsungannya. Warisan nenek moyang yang tetap dijaga sampai kapan 
                        pun.
                     <p>
                 </div>
              </div>
               <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                  <div class="konten-box" data-aos="fade-up">
                        <div class="read-more-container">
                           <div class="container">
                              <p>Pada masa penjajahan Jepang, sekitar tahun 1940-an, tradisi minum Jamu kembali populer karena 
                                 telah dibentuknya komite Jamu Indonesia. Dengan begitu, kepercayaan khasiat terhadap Jamu kembali 
                                 meningkat. Berjalannya waktu, penjualan Jamu pun menyesuaikan dengan teknologi, diantaranya telah 
                                 banyak dikemas dalam bentuk pil, tablet, atau juga bubuk instan yang mudah diseduh. Saat itu berbenturan 
                                 dengan menurunnya kondisi pertanian Indonesia yang mengakibatkan beralihnya ke dunia industri termasuk industri
                                 Jam. </p>
                              <p>Tahun 1974 hingga 1990 banyak berdiri perusahaan Jamu dan semakin berkembang. Pada era itu juga ramai diadakan pembinaan-
                                 pembinaan dan pemberian bantuan dari Pemerintah agar pelaku industri Jamu dapat meningkatkan aktivitas 
                                 produksinya.</p>
                  
                              <span class="read-more-text">
                                 Sejak pertama kali masyarakat Indonesia menggunakan Jamu sebagai minuman kesehatan hingga saat ini, 
                                 pengolahan Jamu berdasarkan ilmu yang diajarkan secara turun-menurun. Namun saat ini, tradisi pengajaran 
                                 pembuatan Jamu telah jarang dilakukan, sehingga penjualan Jamu gendong sudah jarang ditemukan. Sekarang 
                                 ini, semakin sedikit anak muda yang ingin belajar membuat Jamu. Sebagian besar dari mereka berpikir untuk 
                                 mendapatkan Jamu cukup dengan memanfaatkan Jamu yang dijual sachet dan instan.
                                 Perlu diketahui, Jamu dipercaya berasal dari dua kata Jawa Kuno, Djampi yang bermakna 
                                 penyembuhan dan Oesodo yang bermakna kesehatan. Istilah Jamu diperkenalkan ke publik lewat orang-orang 
                                 yang dipercaya punya ilmu pengobatan tradisonal.
                              </span>
                           </p>
                           <span class="read-more-btn">Read More...</span>
                           <div>
                              <img class="imgAdd" src="UAS_PBW_14831_Fatu_R/assets/images/s_9.jpg" alt="image">
                           </div>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </section>