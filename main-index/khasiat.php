<section id="khasiat" class="khasiat" data-aos="fade-up">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Khasiat Jamu </h2>
                     <span>Jamu dibuat dengan bahan dasar rempah dan tanaman herbal yang tentunya memiliki
                         khasiat serta kegunaan pada tubuh. Berikut ini kami tampilkan khasiat jamu, diantaranya:</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="container margin-r-l">
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/1.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/1.jpg" class="fancybox" rel="ligthbox">Membantu mempertahankan imunitas</a>
                        </span>  
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/2.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/2.jpg" class="fancybox" rel="ligthbox">Pereda Nyeri</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/3.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/3.jpg" class="fancybox" rel="ligthbox">Obat Hipertensi</a>
                        </span>
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/4.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/4.jpg" class="fancybox" rel="ligthbox">Obat Asam Urat</a>
                        </span> 
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/5.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/5.jpg" class="fancybox" rel="ligthbox">Penambah Nafsu Makan</a>
                        </span> 
                     </figure>
                  </div>
               </div>
               <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                  <div class="khasiat-box">
                     <figure>
                        <img  src="UAS_PBW_14831_Fatu_R/assets/images/6.jpg" class="zoom img-fluid "  alt="">
                        <span class="hoverle">
                        <a href="UAS_PBW_14831_Fatu_R/assets/images/6.jpg" class="fancybox" rel="ligthbox">Kesehatan dan Kecantikan</a>
                        </span>
                     </figure>
                  </div>
               </div>
            </div>
         </div>
      </section>