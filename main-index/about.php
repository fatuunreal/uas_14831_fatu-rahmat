<section id="contact"class="sectionkonten" data-aos="fade-up">
         <div class="containerkonten">
            <div class="content-section">
               <div class="title">
                  <h1>About Us</h1>
               </div>
               <div class="content">
                  <h3>Hai, Saya Fatu Rahmat</h3>
                  <p>NIM : A11.2022.14831 <br>Kelp: A11.4123<br>Website ini dikembangkan secara mandiri, seluruh konten pada website ini diinput dari pelbagai sumber yang ada di internet.</p>
                  <div class="button">
                     <a href="UAS_PBW_14831_Fatu_R/about-more.html" target="_blank">Read More</a>
                  </div>
               </div>
               <div class="social">
                  <a href="https://facebook.com/fatunreal47" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i>
                  <a href="https://twitter.com/fatuunreal" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i>
                  <a href="https://instagram.com/fatuunreal" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
               </div>
            </div>
            <div class="image-section">
               <img src="UAS_PBW_14831_Fatu_R/assets/images/about.jpg">
            </div>
         </div>
      </section>