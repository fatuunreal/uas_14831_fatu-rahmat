<contact-konten class="slider_contact-konten" data-aos="fade-up">
         <section id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <img class="first-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner2.jpg" alt="First slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Selamat Datang di Website Fatu Rahmat</h1>
                        <p>Website ini dikembangkan untuk menuntaskan nilai mata kuliah Pemrograman Berbasis Web di Universitas Dian Nuswantoro</p>
                        <a  href="#scrolldown">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="second-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner3.jpg" alt="Second slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Sejarah Jamu Tradisonal</h1>
                        <p>Memberikan informasi terhadap asal mula jamu yang menjadi kebudayaan khas Nusantara, mulai dari perkembangannya yang mulai dari
                           kerajaan, hingga kini masih dikonsumsi yang diyakini dapat menjaga stamina serta imunitas tubuh orang Indonesia.
                        </p>
                        <a  href="#sejarah">Read More</a>
                     </div>
                  </div>
               </div>
               <div class="carousel-item">
                  <img class="third-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner4.jpg" alt="Third slide">
                  <div class="container">
                     <div class="carousel-caption relative">
                        <h1>Jamu Tradisional Terpopuler di Indonesia</h1>
                        <p>Berikut kami paparkan jamu tradisionsional terpopuler, dan tentunya hanya ditemukan di Indonesia. </p>
                        <a  href="#topJamu">Read More</a>
                     </div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <i class='fa fa-angle-left'></i>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <i class='fa fa-angle-right'></i>
            </a>
            <a class="bottom_arrow_scroll" href="#konten"><img src="UAS_PBW_14831_Fatu_R/assets/icon/errow.png" /></a>
         </section>
      </contact-konten>