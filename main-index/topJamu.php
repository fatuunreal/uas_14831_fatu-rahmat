<section id="topJamu" class="topJamu" data-aos="fade-up">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Jamu Tradisional Terpopuler</h2>
                     <span>Berikut adalah ragam jenis jamu tradisional yang sangat populer dan sering dikonsumsi masyarakat.</span> 
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <div class="topJamu-box">
                     <figure><img src="UAS_PBW_14831_Fatu_R/assets/images/Top10/berasKencur.jpg" alt="#"/>
                        <span>1. Jamu Beras Kencur</span>
                     </figure>
                     <h3>Jamu Beras Kencur</h3>
                     <p>Jamu beras kencur menjadi salah satu jenis minuman obat tradisional favorit dan disukai banyak orang. 
                        Rasanya yang manis dan segar biasa disajikan hangat atau juga dengan ditambahkan es batu. Bahan jamu beras 
                        kencur adalah kencur, jahe, beras putih, air asam jawa, kunyit, gula pasir, gula jawa, dan air. Cara pembuatan 
                        jamu beras kencur adalah dengan menyangrai dan menghaluskan beras, serta menyiapkan bahan lainnya. Kemudian jamu 
                        diolah dengan cara merebus semua bahan hingga mendidih dan melakukan penyaringan untuk memisahkan ampas sebelum 
                        dikonsumsi. Khasiat jamu beras kencur adalah mengontrol berat badan, menambah nafsu makan, menghilangkan pegal 
                        linu, meningkatkan stamina, sebagai anti diabetes, dan pengontrol berat badan.</p>
                        <div class="read-more">
                           <a href="UAS_PBW_14831_Fatu_R/berasKencur.html">Selengkapnya</a>
                        </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <div class="topJamu-box">
                     <figure><img src="UAS_PBW_14831_Fatu_R/assets/images/Top10/kunyitAsam.jpg" alt="#"/>
                        <span>2. Kunyit Asam</span>
                     </figure>
                     <h3>Kunyit Asam</h3>
                     <p>Kunyit memang dikenal mengandung senyawa curcumenol yang bertugas sebagai analgesik. Sedangkan, kandungan antosianin pada buah asam senyawa yang berfungsi sebagai analgesik atau anti nyeri. Khasiat jamu kunyit asam terkenal bermanfaat melancarkan dan meredakan nyeri haid, serta menjaga kecantikan karena berkhasiat meremajakan sel-sel tubuh. Bahan jamu kunyit asam adalah kunyit, gula aren, garam, asam jawa, dan air. Cara pembuatan jamu adalah dengan menghaluskan kunyit, kemudian merebusnya dengan bagan-bahan lain. Sebelum dikonsumsi, jamu disaring terlebih dulu dan bisa disajikan hangat maupun dingin.
                     </p>
                     <div class="read-more">
                        <a href="UAS_PBW_14831_Fatu_R/kunyitAsam.html">Selengkapnya</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <div class="topJamu-box">
                     <figure><img src="UAS_PBW_14831_Fatu_R/assets/images/Top10/pahitan.jpg" alt="#"/>
                        <span>3. Jamu Pahitan</span>
                     </figure>
                     <h3>Jamu Pahitan</h3>
                     <p>Kebanyakan orang mengenal jamu brotowali sebagai jamu pahitan karena rasanya yang sangat pahit. Walau begitu, bahan jamu pahitan yang utama ternyata daun sambiloto, sementara brotowali, akar alang-alang dan ceplik sari hanya sebagai tambahan saja. Cara pembuatan jamu adalah dengan cara merebus untuk mengambil saripatinya. Khasiat jamu ini terkenal memiliki banyak manfaat mulai dari mengatasi pegal-pegal, menyembuhkan penyakit gatal-gatal, menambah nafsu makan, mencegah risiko diabetes, terapi cuci darah, dan anti alergi.
                     </p>
                     <div class="read-more">
                        <a href="UAS_PBW_14831_Fatu_R/pahitan.html">Selengkapnya</a>
                     </div>
                  </div>
               </div>
               <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                  <div class="topJamu-box">
                     <figure><img src="UAS_PBW_14831_Fatu_R/assets/images/Top10/wedangJahe.jpg" alt="#"/>
                        <span>4. Wedang Jahe</span>
                     </figure>
                     <h3>Wedang Jahe</h3>
                     <p>Wedang jahe adalah olahan jamu yang paling mudah dan banyak dikreasikan menjadi berbagai minuman berkhasiat. Bahan jamu yang utama adalah rimpang jahe yang dimemarkan kemudian direbus atau diseduh. Cara pembuatan jamu tergantung selera, yaitu dicampur dengan gula merah, susu kental manis, atau menjadi bahan olahan wedang ronde, bajigur, maupun bandrek.
                     </p>
                     <div class="read-more">
                           <a href="UAS_PBW_14831_Fatu_R/wedangJahe.html">Selengkapnya</a>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </section>