<section id="gallery" class="gallery" data-aos="fade-up">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Gallery</h2>
                     <span>Potret kebudayaan bangsa, tercipta akan rasa serta karsa. Melebur dalam kekayaan Nusantara, berkat pemberian Yang Maha Kuasa</span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div id="main_slider" class="carousel slide banner-main" data-ride="carousel">
                     <div class="carousel-inner">
                        <div class="carousel-item active"> <img class="first-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner.jpg" alt="First slide"> </div>
                        <div class="carousel-item"> <img class="second-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner5.jpg" alt="Second slide"> </div>
                        <div class="carousel-item"> <img class="third-slide" src="UAS_PBW_14831_Fatu_R/assets/images/banner6.jpg" alt="Third slide"> </div>
                     </div>
                     <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev"> <i class='fa fa-angle-left'></i></a> <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next"> <i class='fa fa-angle-right'></i></a> 
                  </div>
                  <div class="read-more">
                     <a href="UAS_PBW_14831_Fatu_R/gallery-more.html">Selengkapnya</a>
                  </div>
               </div>
            </div>
         </div>
      </section>